# Mike's Ideal Filesystem

A reference directory tree to remind me of where I want to get my personal data repository to in terms of organisation.

Run `tree -a -I '*git*' -C` to get a good view of the layout

Thanks [r/datacurator](https://www.reddit.com/r/datacurator/top/?t=all).
